<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * 统一返回信息
 * @param $code
 * @param $data
 * @param $msge
 */
function easymsg($code, $data, $msg)
{
    return compact('code', 'data', 'msg');
}

function msg($message){
        $str = '<script>';
        $str .='parent.error("' . $message . '" );';
        $str.='</script>';
        exit($str);
}

function sures($message, $jumpUrl = ''){
    $str = '<script>';
        $str .='parent.success("' . $message . '",\'jumpUrl("' . $jumpUrl . '")\');';
        $str.='</script>';
        exit($str);
}