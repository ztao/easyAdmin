/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : easyadmin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-05-14 15:20:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ky_good
-- ----------------------------
DROP TABLE IF EXISTS `ky_good`;
CREATE TABLE `ky_good` (
  `good_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NOT NULL COMMENT '商品分类',
  `good_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '产品名称',
  `good_img` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '商品图片',
  `context` text COLLATE utf8_unicode_ci COMMENT '产品详细',
  `price` decimal(10,0) DEFAULT NULL,
  `creattime` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `good_s_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品短标题',
  `mall_price` decimal(10,2) NOT NULL COMMENT '商城价',
  `cost_price` decimal(10,2) DEFAULT NULL COMMENT '成本价',
  PRIMARY KEY (`good_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ky_good
-- ----------------------------
INSERT INTO `ky_good` VALUES ('1', '1', '测试水果', 'good/5af9368d2cdf8.jpg', null, '0', '1526281878', '', '15.23', '0.00');

-- ----------------------------
-- Table structure for ky_good_cate
-- ----------------------------
DROP TABLE IF EXISTS `ky_good_cate`;
CREATE TABLE `ky_good_cate` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '分类名称',
  `orderby` tinyint(11) DEFAULT '100' COMMENT '排序',
  `parent_id` int(11) DEFAULT '0' COMMENT '上级ID',
  `remark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`cate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ky_good_cate
-- ----------------------------
INSERT INTO `ky_good_cate` VALUES ('1', '水果', '100', '0', '水果分类');
INSERT INTO `ky_good_cate` VALUES ('6', '水2', '0', '5', '');
INSERT INTO `ky_good_cate` VALUES ('5', '水1', '0', '1', '');

-- ----------------------------
-- Table structure for ky_system_dept
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_dept`;
CREATE TABLE `ky_system_dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门名称',
  `parent_id` tinyint(4) DEFAULT '0' COMMENT '上级ID',
  `dept_cont` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `orderby` tinyint(4) DEFAULT '100' COMMENT '排序',
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ky_system_dept
-- ----------------------------
INSERT INTO `ky_system_dept` VALUES ('1', '技术部', '0', 'JAVA技术部', '1');
INSERT INTO `ky_system_dept` VALUES ('2', 'Java技术部', '1', 'Java技术部', '1');
INSERT INTO `ky_system_dept` VALUES ('3', 'PHP技术部', '1', 'PHP技术部', '2');
INSERT INTO `ky_system_dept` VALUES ('4', '前端', '1', 'CSS+HTML+JAVASCRIPT', '3');
INSERT INTO `ky_system_dept` VALUES ('5', '销售部', '0', '公司销售部', '2');
INSERT INTO `ky_system_dept` VALUES ('6', '销售一部', '5', '主要销售JAVA系统', '1');
INSERT INTO `ky_system_dept` VALUES ('7', '客户', '0', '客户', '3');
INSERT INTO `ky_system_dept` VALUES ('8', '微信客户', '7', '微信客户', '1');
INSERT INTO `ky_system_dept` VALUES ('9', 'APP客户', '7', 'APP客户', '2');
INSERT INTO `ky_system_dept` VALUES ('10', '实体客户', '7', '实体客户', '3');

-- ----------------------------
-- Table structure for ky_system_log
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_log`;
CREATE TABLE `ky_system_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- ----------------------------
-- Records of ky_system_log
-- ----------------------------

-- ----------------------------
-- Table structure for ky_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_menu`;
CREATE TABLE `ky_system_menu` (
  `menu_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` char(80) NOT NULL DEFAULT '菜单名称',
  `menu_role` char(20) NOT NULL DEFAULT '权限标识',
  `type` tinyint(1) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  `condition` char(100) DEFAULT '',
  `menu_icon` varchar(45) DEFAULT NULL COMMENT '图标',
  `parent_id` int(11) DEFAULT '0' COMMENT '上级Id',
  `orderby` tinyint(4) DEFAULT '100',
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `menu_role` (`menu_role`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ky_system_menu
-- ----------------------------
INSERT INTO `ky_system_menu` VALUES ('1', '系统设置', 'system', '1', '1', '', 'md-settings', '0', '1');
INSERT INTO `ky_system_menu` VALUES ('2', '系统菜单', 'menu/index', '1', '1', '', 'md md-list', '1', '1');
INSERT INTO `ky_system_menu` VALUES ('10', '角色管理', 'role/index', '1', '1', '', 'md md-account-child', '1', '2');
INSERT INTO `ky_system_menu` VALUES ('7', '添加', 'menu/add', '1', '1', '', '', '2', '1');
INSERT INTO `ky_system_menu` VALUES ('8', '修改', 'menu/edit', '1', '1', '', '', '2', '2');
INSERT INTO `ky_system_menu` VALUES ('9', '删除', 'menu/del', '1', '1', '', '', '2', '3');
INSERT INTO `ky_system_menu` VALUES ('11', '添加', 'role/add', '1', '1', '', '', '10', '1');
INSERT INTO `ky_system_menu` VALUES ('12', '删除', 'role/del', '1', '1', '', '', '10', '3');
INSERT INTO `ky_system_menu` VALUES ('13', '修改', 'role/edit', '1', '1', '', '', '10', '2');
INSERT INTO `ky_system_menu` VALUES ('14', '系统菜单图标', 'menu/icon', '1', '1', '', '', '2', '4');
INSERT INTO `ky_system_menu` VALUES ('15', '系统菜单列表', 'menu/menulist', '1', '1', '', '', '2', '5');
INSERT INTO `ky_system_menu` VALUES ('16', '会员管理', 'user', '1', '1', '', 'md md-account-circle', '0', '2');
INSERT INTO `ky_system_menu` VALUES ('17', '部门管理', 'dept/index', '1', '1', '', 'md md-assignment-ind', '16', '1');
INSERT INTO `ky_system_menu` VALUES ('18', '添加', 'dept/add', '1', '1', '', '', '17', '1');
INSERT INTO `ky_system_menu` VALUES ('19', '修改', 'dept/edit', '1', '1', '', '', '17', '2');
INSERT INTO `ky_system_menu` VALUES ('20', '删除', 'dept/del', '1', '1', '', '', '17', '3');
INSERT INTO `ky_system_menu` VALUES ('21', '会员列表', 'user/index', '1', '1', '', 'md md-account-child', '16', '2');
INSERT INTO `ky_system_menu` VALUES ('22', '添加', 'user/add', '1', '1', '', '', '21', '1');
INSERT INTO `ky_system_menu` VALUES ('23', '修改', 'user/edit', '1', '1', '', '', '21', '2');
INSERT INTO `ky_system_menu` VALUES ('24', '删除', 'user/del', '1', '1', '', '', '21', '3');
INSERT INTO `ky_system_menu` VALUES ('25', '日志管理', 'logs', '1', '1', '', 'md md-assignment-late', '0', '12');
INSERT INTO `ky_system_menu` VALUES ('26', '日志列表', 'logs/index', '1', '1', '', 'md md-info', '25', '1');
INSERT INTO `ky_system_menu` VALUES ('27', '清空日志', 'logs/del', '1', '1', '', '', '26', '1');
INSERT INTO `ky_system_menu` VALUES ('28', '商城管理', 'mall', '1', '1', '', 'md md-shopping-basket', '0', '4');
INSERT INTO `ky_system_menu` VALUES ('29', '商品管理', 'good/index', '1', '1', '', 'md md-stars', '28', '1');
INSERT INTO `ky_system_menu` VALUES ('30', '添加', 'good/add', '1', '1', '', '', '29', '1');
INSERT INTO `ky_system_menu` VALUES ('31', '修改', 'good/edit', '1', '1', '', '', '29', '2');
INSERT INTO `ky_system_menu` VALUES ('32', '删除', 'good/del', '1', '1', '', '', '29', '3');
INSERT INTO `ky_system_menu` VALUES ('33', '商品分类', 'goodcate/index', '1', '1', '', 'md md-bookmark', '28', '2');
INSERT INTO `ky_system_menu` VALUES ('34', '添加', 'goodcate/add', '1', '1', '', '', '33', '1');
INSERT INTO `ky_system_menu` VALUES ('35', '修改', 'goodcate/edit', '1', '1', '', '', '33', '2');
INSERT INTO `ky_system_menu` VALUES ('36', '删除', 'goodcate/del', '1', '1', '', '', '33', '3');
INSERT INTO `ky_system_menu` VALUES ('37', '微信设置', 'weixin', '1', '1', '', 'md md-leak-add', '0', '5');
INSERT INTO `ky_system_menu` VALUES ('38', '微信配置', 'weixin/index', '1', '1', '', 'md md-multitrack-audio', '37', '1');
INSERT INTO `ky_system_menu` VALUES ('39', '配送管理', 'shopper', '1', '1', '', 'md md-account-box', '0', '6');
INSERT INTO `ky_system_menu` VALUES ('40', '订单管理', 'orders', '1', '1', '', 'md md-assignment', '0', '7');
INSERT INTO `ky_system_menu` VALUES ('41', '统计管理', 'census', '1', '1', '', 'md md-apps', '0', '8');
INSERT INTO `ky_system_menu` VALUES ('42', '菜单权限列表', 'menu/tree', '1', '1', '', '', '2', '6');

-- ----------------------------
-- Table structure for ky_system_role
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_role`;
CREATE TABLE `ky_system_role` (
  `role_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ky_system_role
-- ----------------------------
INSERT INTO `ky_system_role` VALUES ('1', '超级管理员', '1', '7,8,9,14,15,42,2,11,12,13,10,1,18,19,20,17,22,23,24,21,27,26,30,31,32,34,35,36,16,25,29,33,28,0,38,37,39,40,41');
INSERT INTO `ky_system_role` VALUES ('19', '编辑员', '1', '7,8,9,14,15,42,2,11,12,13,10,1,18,19,20,17,22,23,24,21,16,27,26,25,0');

-- ----------------------------
-- Table structure for ky_system_user
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_user`;
CREATE TABLE `ky_system_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登录名称',
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登录密码',
  `nickname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '妮称',
  `face` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `phone` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话号码',
  `login_ip` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '登录IP',
  `login_time` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) DEFAULT '0' COMMENT '状态',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `login_num` tinyint(4) DEFAULT NULL COMMENT '登录次数',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `creattime` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ky_system_user
-- ----------------------------
INSERT INTO `ky_system_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Mr.He', null, '18223830067', '127.0.0.1', '1526281087', '1', '1', '49', '1', '1526026790');

-- ----------------------------
-- Table structure for ky_system_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_user_role`;
CREATE TABLE `ky_system_user_role` (
  `uid` mediumint(8) unsigned NOT NULL,
  `role_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_role_id` (`uid`,`role_id`),
  KEY `uid` (`uid`),
  KEY `role_id` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ky_system_user_role
-- ----------------------------
INSERT INTO `ky_system_user_role` VALUES ('3', '19');
